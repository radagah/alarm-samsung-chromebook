<h1>Touchpad and Audio fix for Samsung Chromebook on Arch Arm</h1>

<b>NOT MAINTAINED!</b>
<br>
<p>These are just 2 PKGBUILDs for the Samsung Chromebook using Arch Linux Arm.</p>
<p>They are intended to enable audio by default and to add the touchpad configuration.</p>
<p>Please, read the code, since I haven't used the packages in a while. They might be outdated.'.</p>
<br>
<b>INSTALLATION:</b>
<p>The installation is done just like any AUR package without an AUR manager.</p>
<p>Open the directory with the package files and type 'makepkg -sri', and if it doesn't work, use 'makepkg -fsri'.</p>